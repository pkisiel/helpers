            try
            {
                // First DocumentCompleted 
                Bitmap bitmap = new Bitmap(webBrowser1.Width, webBrowser1.Height);
                webBrowser1.DrawToBitmap(bitmap, new Rectangle(0, 0, webBrowser1.Width, webBrowser1.Height));
                string file = (string)(DateTime.UtcNow.Subtract(new DateTime(1970, 1, 1))).TotalSeconds.ToString();
                // the directory to store the output.
                string directory = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);
                string path = Path.Combine(directory, file + ".bmp");
                bitmap.Save(path);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
            